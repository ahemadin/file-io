I used some stackoverflow/tutorials to write this.

To understand how we can write getters/setters in Java under the object oriented programming model, I used the bicycle example provided in the java documentation. I don't remember the exact link, but the syntax here is good

https://docs.oracle.com/javase/tutorial/java/javaOO/classes.html

I think in order for you to understand how this all works, you should look up how to work with classes, constructors, and creating new objects. 



To work with fileIO, I used a tutorial. Stack overflow has pretty good links as well. Be sure to use those as a refernce. 

https://www.caveofprogramming.com/java/java-file-reading-and-writing-files-in-java.html