// Importing our java IO utilities.
import java.io.*;

// Creating a class called animal. This has Two attributes and Four Methods
public class Animal {

    public String color;
    public String name;

    public Animal(String newColor, String newName) {
        color = newColor;
        name = newName;
    }

    public void setColor(String newColor) {
        color = newColor;
    }

    public void setName(String newName) {
        name = newName;
    }

    public String getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    public static void main(String[] args) {
        // First let us create a new animal

        // myColor= "red";
        //  = "red";
        String color = "red";
        String name = "touchme";
        Animal newAnimal = new Animal(color, name);

        // newAnimal.setColor("Red");
        // newAnimal.setName("Bee");
        // // This creates an animal with a string red for color, and a string "bee" for name of the animal

        // Now let's check the color of our newAnimal;
        System.out.println("The color of newAnimal is: ");
        System.out.println(newAnimal.color);

        System.out.println("The name of newAnimal is: ");
        System.out.println(newAnimal.name);

        // But now I want to change the color of my animal.
        newAnimal.setColor("Pink");
        System.out.println("The color of newAnimal is: ");
        System.out.println(newAnimal.color);

        // And if I want to change my name?
        newAnimal.setName("Reid");
        System.out.println("The name of newAnimal is: ");
        System.out.println(newAnimal.name);

        // Now let's make an animal using input from a file. 

        // My idea is to read the file line by line. We can have the variables set in the file we're reading from
        //I saved a file called animal.txt with two lines. one line is 'red', one is 'bee'
        //Note that I've imported all java.io.* at the top.

        // Our FileIO requires Try/Catch Blocks

        try {
            //Create a new BufferedREader object called reader
            //This object reads in a file "animal.txt"
            BufferedReader reader = new BufferedReader(new FileReader("animal.txt"));
            //Then we read one line of the file and set our color to it

            // Our setColor method sets the color attribute of the object to whatever argument is given. In this case
            // It is the first line of the txt doc
            newAnimal.setColor(reader.readLine());
            newAnimal.setName(reader.readLine());

            // If there is an error we will print it.
        } catch (IOException e) {
            e.printStackTrace();
        }

        // HERE IS WHERE WE WRITE TO A FILE

        // Every time we use File IO, we need a try/catch block

        try {
            // Create a new PrintWriter object called writer
            PrintWriter writer = new PrintWriter("output.txt", "UTF-8");

            // writer.println writes to the next line in the file

            // Here we're writing the name and color of the animal using our newAnimal.getName() method.
            writer.println("The animal is a " + newAnimal.getName());
            writer.println("The Color is " + newAnimal.getColor());
            writer.close();
        }

        catch (IOException ex) {
            System.out.println("IO EXCEPTION OCCURED");
        }

    }

}